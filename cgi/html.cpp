#include "html.h"
#include <iostream>
#include <cstring>
using namespace std;

void printHTMLHeader(int colCount, const char *title[]) {
	

	cout << 
		"<!DOCTYPE html>\n"
		"<html>\n"
		"<head>\n"
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\n"
		"<title>Network Programming Homework 3</title>\n"
		"</head>\n"
		"<body bgcolor=#336699>\n"
		"<font face=\"Courier New\" size=2 color=#FFFF99>\n"
		"<table width=\"800\" border=\"1\">\n"
		"<tr>\n";
	
	for (int i=0; i<colCount; i++) {
		cout <<"<td>"<< title[i] <<"</td>";
	}
	cout <<"</tr>\n";
	
	for (int i=0; i<colCount; i++) {
		cout <<"<td valign=\"top\" id=\"m" << i << "\"></td>";
	}
	cout<<"\n</table>\n";

}

void printLine(int col, const char *str, bool flag) {
	//<script>document.all['m0'].innerHTML += "****************************************<br>";</script>
	cout <<"<script>document.all['m"<< col << "'].innerHTML += \"";
	if(flag) cout<<"<b>";

	int pos = 0;
	int len = strlen(str);
	while (pos != len) {//let some special mark print on client,but need to let httpd recognize it
		switch (str[pos]) {
			case '\n':
				if(flag) cout<<"</b>";
				cout << "<br>";
				break;
			case '\r'://file may contain this!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				break;
			/*case '"':
				cout << "\\\"";
				break;
			case '\\':
				cout << "\\\\";
				break;*/
			case '"':
				cout<<"&quot;";
				break;
			case '>':
				cout<<"&gt;";
				break;
			case '<':
				cout<<"&lt;";
				break;
			case ' ':
				cout<<"&nbsp;";
				break;
			default:
				cout << str[pos];
		}
		
		++pos;
	}
	//cout<<"<br>";

	cout << "\";</script>" << endl;
}

void printHTMLFooter() {
	cout << 
		"</font>\n"
		"</body>\n"
		"</html>" << endl;
}
