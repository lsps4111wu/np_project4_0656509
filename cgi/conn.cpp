#include"conn.h"
#include"html.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <cstring>
#include<iostream>
using namespace std;

Connection::Connection(TaskList &t, int id_num, const char *host, int port, const char *filename,const char* s_host, unsigned short s_port) : has_closed(false),id(id_num), task_list(t), file(filename, ios::in)
{
	//check args validation
	hostent *host_dat = gethostbyname(host);
	if(host_dat == NULL) {
		print("Unable to find host.");
		return;
	}
	//print("able to find host.\n");
	if(filename == NULL){
		print("Unable to open file.");
		return;
	}
	cout<<"s_host = "<<s_host<<endl;
	cout<<"s_host len:"<<strlen(s_host)<<endl;
	if(s_host){
		need_SOCKS = true;
		cout<<"need_SOCKS\n";
	} else {
		need_SOCKS = false;
		cout<<"no need_SOCKS\n";
	}
	
	//set sockaddr struct
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	
	if(need_SOCKS){//pack the sock4 request
		sock4_data.VN = 0x04;
		sock4_data.CD = 0x01;
		sock4_data.port = htons(port);
		memcpy(&sock4_data.ip, host_dat->h_addr, sizeof(sock4_data.ip));//destination IP
		sock4_data.nul = 0x00;

		host_dat = gethostbyname(s_host);
		if(host_dat == NULL){
			print("Unable to find SOCKS host.");
			return;
		}

		//fill in SOCK server data
		addr.sin_port = htons(s_port);
		memcpy(&addr.sin_addr, host_dat->h_addr, host_dat->h_length);

	} else {//normal case
		addr.sin_port = htons(port);
		memcpy(&addr.sin_addr, host_dat->h_addr, host_dat->h_length);
	}


	//create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		print("socket error");
		return;
	}
	
	//set non-blocking flag
	int my_flag = fcntl(sockfd, F_GETFL, 0);
	fcntl(sockfd, F_SETFL, my_flag | O_NONBLOCK);

	do_connect();
}

void Connection::do_connect() {
	socklen_t len = sizeof(addr);
	//cout<<sockfd<<endl;
	int rc = connect(sockfd, (sockaddr *) &addr, len);
	//print("1");
	if(rc == 0 || errno == EISCONN){//if connect success or already connect to server, read the welcome msg or sth...
		if(need_SOCKS){
			printf("SOCKS request start");
			do_request();
		} else {
			printf("read");
			do_read();
		}
	} else {
		print(strerror(errno));
		print("\n");
		if(errno == EALREADY || errno == EINPROGRESS){//connect it later
			//EALREADY : The socket is nonblocking and a previous connection attempt has not yet been completed.
			//EINPROGRESS : The socket is nonblocking and the connection cannot be completed immediately.		
			
			//give to callback
			printf("connect later\n");
			task_list.addWriteTask(sockfd,NULL,0,ConnectCallback,this);
		} else {
			printf("connect case error");
		}
	}
}

void Connection::ConnectCallback(void *arg, ssize_t) {
	Connection* this_ptr = (Connection*) arg;
	this_ptr->do_connect();
}

void Connection::do_write() {
	if(!file){
		file.close();
	} else {
		getline(file,fileLine);
		fileLine.push_back('\n');
		////cout<<"\"\"\"\"\"\"\"\"\"\"\"theflag = "<<has_closed<<endl;
		if(! has_closed) print(fileLine.c_str(),true);
		task_list.addWriteTask(sockfd,(void*) fileLine.c_str(),fileLine.length(),WriteCallback,this);
		////cout<<"cmd line contenttttttttttt"<<fileLine<<endl;
		if(! fileLine.compare("exit\n") ){
			has_closed = true;
		}
	}
}

void Connection::WriteCallback(void *arg, ssize_t) {
	Connection* this_ptr = (Connection*) arg;
	this_ptr->do_read();
}

void Connection::do_read() {
	printf("in do_read\n");
	task_list.addReadTask(sockfd,(void*) read_buf,sizeof(read_buf)-1,ReadCallback,this);
}

void Connection::ReadCallback(void *arg, ssize_t n) {
	Connection* this_ptr = (Connection*) arg;
	printf("in ReadCallback\n");
	//cout<<"n="<<n<<' '<<endl;
	if(n>0){
		this_ptr->read_buf[n] = '\0';
		this_ptr->print(this_ptr->read_buf);

		bool find_EOF=false;
		for(int i=0;i<n;++i){
			if( this_ptr->read_buf[i]=='%' && this_ptr->read_buf[i+1]==' ' ){//I got the "% ",read done
				find_EOF=true;
				this_ptr->do_write();
				break;
			}
		}

		if( !find_EOF ){//continue to read
			//cout<<" do read again "<<endl;
			this_ptr->do_read();
		}
	} else {
		printf("close socket\n");
		close(this_ptr->sockfd);
	}
}

void Connection::print(const char *str, bool flag) {
	::printLine(id,str,flag);
}


void Connection::do_request() {
	printf("in do_request\n");
	task_list.addWriteTask(sockfd,(void*) &sock4_data,sizeof(sock4_data),RequestCallback,this);
}

void Connection::RequestCallback(void *arg, ssize_t n) {
	Connection* this_ptr = (Connection*) arg;
	this_ptr->do_response();
}

void Connection::do_response() {
	printf("in do_response\n");
	task_list.addReadTask(sockfd,(void*) &sock4_data,8,ResponseCallback,this);
}

void Connection::ResponseCallback(void *arg, ssize_t n) {
	Connection* this_ptr = (Connection*) arg;
	
	if(this_ptr->sock4_data.CD == 0x5A){//accepted
		this_ptr->do_read();
	} else {
		this_ptr->print("SOCKS rejected.");
	}
}