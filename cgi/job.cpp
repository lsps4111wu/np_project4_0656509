#include "job.h"
#include <fcntl.h>
#include <unistd.h>
#include <sys/select.h>
#include <cstdio>
#include <iostream>
using namespace std;

void TaskList::addReadTask(int fd, void *buf, size_t maxRead, Callback callback_func, void *callback_arg)
{
	addTask(Task::Read_type,fd,buf,maxRead,callback_func,callback_arg);
}

void TaskList::addWriteTask(int fd, void *buf, size_t nWrite, Callback callback_func, void *callback_arg) 
{
	addTask(Task::Write_type,fd,buf,nWrite,callback_func,callback_arg);
}

void TaskList::loop()
{
	fd_set read_set,write_set;
	//cout<<"in loop"<<endl;
	while( setCheckSet(&read_set,&write_set) ){//set the fd set I am interested
		cout<<"in \"while loop\""<<endl;
		select(maxfd+1,&read_set,&write_set,NULL,NULL);
		//sleep(1);
		checkDone(&read_set,&write_set);//check the event status
		cout<<" this loop is ended "<<endl;
	}
}

void TaskList::addTask(Task::Type type, int fd, void *buf, size_t n, Callback callback_func, void *callback_arg)
{
	//create new task
	Task t;
	t.type = type;
	t.fd = fd;
	t.buf = buf;
	t.size = n;
	t.callback_func = callback_func;
	t.callback_arg = callback_arg;
	t.current_finish_size = 0;//

	//add non-blocking flag to fd
	int fd_flag = fcntl(fd,F_GETFL);
	fcntl(fd,F_SETFL,fd_flag | O_NONBLOCK);
	//cout<<"in addTask"<<endl;
	//cout<< (type == Task::Read_type)? "Read_type " : "Write_type" ;
	//add to list
	task_list.push_front(t);
	//cout<<"task_list size = "<<task_list.size()<<endl;
	//update maxfd for select
	if(fd > maxfd) maxfd = fd;

}

int TaskList::setCheckSet(fd_set *read_set, fd_set *write_set)
{
	FD_ZERO(read_set);
	FD_ZERO(write_set);
	//cout<<"in setCheckSet"<<endl;
	for(list<Task>::iterator it = task_list.begin(); it != task_list.end(); ++it){
		
		if(fcntl(it->fd,F_GETFD) == -1){//the fd doesn't exist, erase it
			//cout<<"fcntl(it->fd,F_GETFD) == -1"<<endl;
			task_list.erase(it);
			continue;
		}

		if(it->type == Task::Read_type){
			//cout<<"Read_type"<<endl;
			FD_SET(it->fd,read_set);
			//print("Read_type\n");
		} 
		else if(it->type == Task::Write_type){
			//cout<<"Write_type"<<endl;
			FD_SET(it->fd,write_set);
			//print("Write_type\n");
		} 
		else {
			//cout<<"crazzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy\n"<<flush;
		}
	}
	//cout<<"task_list size = "<<task_list.size()<<endl;
	return task_list.size();
}

void TaskList::checkDone(fd_set *read_set, fd_set *write_set)
{
	//cout<<"in checkDone "<<endl;
	//cout<<"task_list size = "<<task_list.size()<<endl;
	for(list<Task>::iterator it = task_list.begin(); it != task_list.end(); ){
		ssize_t n = -1;

		if(it->type == Task::Read_type && FD_ISSET(it->fd,read_set)){//this fd want to read
			//cout<<"in it->type == Task::Read_type && FD_ISSET(it->fd,read_set) "<<endl;
			n = read(it->fd, (char*) it->buf + it->current_finish_size, it->size - it->current_finish_size);
			//cout<<"read char num = "<<n<<' '<<endl;
			if(n==0){//is close msg, erase the task from list
				(*it->callback_func)(it->callback_arg,0);
				it = task_list.erase(it);
				++it;
				continue;
			}
		} else if(it->type == Task::Write_type && FD_ISSET(it->fd,write_set)){//this fd want to write
			//cout<<"in \"it->type == Task::Write_type && FD_ISSET(it->fd,write_set)\" "<<endl;
			if(it->size > 0){//write to target server (from the last stop point)
				n = write(it->fd, (char*) it->buf + it->current_finish_size, it->size - it->current_finish_size);
			} else {
				//cout<<"n=0 "<<endl;
				n = 0;
			}
		}

		//for(list<Task>::iterator ita = task_list.begin(); ita != task_list.end(); ++ita);
		if(n==-1){//may be 1.read nothing 2.write nothing, not finish
			++it;
		} else if( it->type == Task::Write_type && (it->current_finish_size += n ) != it->size ){//just write part of file, not finish
			++it;
		} else {//the task has done, erase it
			//cout<<"task done OAO "<<endl;
			(*it->callback_func)(it->callback_arg, n);
			//cout<<"callback_func done "<<endl;
			it = task_list.erase(it);
			//cout<<"task_list size = "<<task_list.size()<<endl;
			//++it;
		}
	}
	//cout<<"checkDone ENDDDDDDDDDDDDDD"<<endl;
}
