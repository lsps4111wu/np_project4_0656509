#include"html.h"
#include"job.h"
#include"conn.h"

#include<iostream> 
#include<string>
#include<cstring> 
#include <cstdlib>
using namespace std;

#define MAX_QUERY_NUM 5

struct ServerInfo {
	char host[100];
	char port[10];
	char file[100];
	char shost[100];
	char sport[10];
}; 


void filename_decode(char *dst, const char *src) {
	int len = strlen(src);
	int j=0;
	for (int i=0; i<len; ++i, ++j) {
		
		switch (src[i]) {
			case '%'://maybe have special mean
				if (isxdigit(src[i+1]) && isxdigit(src[i+2])) {
					int ascii;
					sscanf(src+ i+1, "%2x", &ascii);
					dst[j] = (char) ascii;
					i += 2;
				} else {
					dst[j] = '%';
				}
				break;
			case '+'://means space in html
				dst[j] = ' ';
				break;
			default:
				dst[j] = src[i];
		}
	}

	dst[j] = '\0';
}

int main()
{
	//char query_string[]="h1=nplinux1.cs.nctu.edu.tw&p1=5555&f1=t1.txt&sh1=nplinux1.cs.nctu.edu.tw&sp1=9999&h2=&p2=&f2=&sh2=&sp2=";
	cout << "Content-type: text/html\n\n" << flush; 
	char* query_string= getenv("QUERY_STRING");
	cout<<query_string<<endl<<flush;
	string str(query_string);
	//cout<<str<<"1"<<flush;
	int server_num = 0;
	ServerInfo all_server[MAX_QUERY_NUM];

	for(int i=0;i<MAX_QUERY_NUM;++i){
		memset(all_server[i].port,0,10);
		memset(all_server[i].sport,0,10);
		memset(all_server[i].file,0,100);
		memset(all_server[i].host,0,100);
		memset(all_server[i].shost,0,100);
	}
	
	while(!str.empty()){
		int place = str.find_first_of("&");
		if(place != string::npos){
			string sub_str;
			char flag = str[0];
			cout<<"flag = "<<flag<<endl;
			if(flag == 'h'){
				sub_str += str.substr(3,place-3);//just want the value
				str.erase(0,place+1);
				cout <<"h = "<<sub_str<<endl;
				strncpy(all_server[server_num].host,sub_str.c_str(),100);
				cout<<all_server[server_num].host<<endl;
			} else if(flag == 'p'){
				sub_str += str.substr(3,place-3);//just want the value
				str.erase(0,place+1);
				cout <<"p = "<<sub_str<<endl;
				strncpy(all_server[server_num].port,sub_str.c_str(),10);
				cout << strlen(all_server[server_num].port);
				cout<<all_server[server_num].port<<endl;
			} else if(flag == 'f'){
				sub_str += str.substr(3,place-3);//just want the value
				str.erase(0,place+1);
				cout <<"f = "<<sub_str<<endl;
				strncpy(all_server[server_num].file,sub_str.c_str(),100);
				cout<<all_server[server_num].file<<endl;
				
			} else if(flag == 's'){
				if(str[1] == 'h'){
					sub_str += str.substr(4,place-4);//just want the value
					cout<<"shost = ";
					str.erase(0,place+1);
					strncpy(all_server[server_num].shost,sub_str.c_str(),100);
					cout<<all_server[server_num].shost<<endl;
				} else if(str[1] == 'p'){
					sub_str += str.substr(4,place-4);//just want the value
					cout<<"sport = ";
					str.erase(0,place+1);
					strncpy(all_server[server_num].sport,sub_str.c_str(),10);
					cout<<all_server[server_num].sport<<endl;
					++server_num;
				}
			}
		} else {
			cout <<"sport = "<<str<<endl;
			str.erase(0,4);
			strncpy(all_server[server_num].sport,str.c_str(),100);
			cout<<all_server[server_num].sport<<endl;
			++server_num;
			break;
		}
		cout<<endl;
	}
	
	for(int i=0;i<5;++i){
		cout<<"port = "<<all_server[i].port<<endl;
		cout<<"host = "<<all_server[i].host<<endl;
		cout<<"file = "<<all_server[i].file<<endl;
		cout<<"socks host = "<<all_server[i].shost<<endl;
		cout<<"socks port = "<<all_server[i].sport<<endl;
	}


	ServerInfo* servers_reduced[server_num];//omit the empty server
	const char* titles_for_page[server_num];//print server address
	
	for (int i=0, j=0; i<=MAX_QUERY_NUM ; ++i) {
		if (strlen(all_server[i].host)>0) {
			titles_for_page[j] = all_server[i].host;
			//servers_reduced[j] = &all_server[i];
			++j;

		}
	}
	//printHTMLHeader(5);

	printHTMLHeader(server_num, titles_for_page);
	
	/*char dst[10];
	char src[]="123%3x%3D";
	filename_decode(dst,src);
	cout<<dst<<endl; */
	
	for (int i=0; i<5; ++i) {
		cout<<all_server[i].port<<endl;
	}


	TaskList task_list;
	Connection *connections[5];//setup all connections to target server
	/*for (int i=0; i<server_num; ++i) {
		char file_name[strlen( servers_reduced[i]->file) + 1];
		filename_decode(file_name, servers_reduced[i]->file);//get the input filename from html
		cout<<servers_reduced[i]->port<<endl;
		cout<<atoi(servers_reduced[i]->port)<<endl;
		int p = atoi(servers_reduced[i]->port);

		connections[i] = new Connection(task_list, i,servers_reduced[i]->host, p,file_name);
	}*/
	for (int i=0; i<5; ++i) {
		char file_name[strlen( all_server[i].file) + 1];
		filename_decode(file_name, all_server[i].file);//get the input filename from html
		//cout<<all_server[i].port<<endl;
		cout<<atoi(all_server[i].port)<<endl;
		int p = atoi(all_server[i].port);

		connections[i] = new Connection(task_list, i,all_server[i].host, p,file_name,all_server[i].shost,(unsigned short) (all_server[i].sport ? atoi(all_server[i].sport) : 0) );
	}

	task_list.loop();
	printHTMLFooter();
}
