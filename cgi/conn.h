#pragma once

#include "job.h"
#include <netinet/in.h>
#include <fstream>
#include <string>

class Connection {
public:
	Connection(TaskList& , int , const char *, int , const char *, const char*, unsigned short);

private:
	void do_connect();
	static void ConnectCallback(void *, ssize_t);
	void do_write();
	static void WriteCallback(void *, ssize_t);
	void do_read();
	static void ReadCallback(void *, ssize_t);
	void print(const char *,bool flag = false);

	//hw4 for SOCKS
	void do_request();
	static void RequestCallback(void *, ssize_t);
	void do_response();
	static void ResponseCallback(void *, ssize_t);


	int id;
	TaskList& task_list;
	int sockfd;
	sockaddr_in addr;//save server data
	std::ifstream file;//read input file
	std::string fileLine;//a line of file
	char read_buf[10000];
	bool has_closed;
	bool need_SOCKS;

	struct SOCK4
	{
		unsigned char VN;
		unsigned char CD;
		unsigned short port;
		unsigned char ip[4];
		unsigned char nul;
	} sock4_data;
};
