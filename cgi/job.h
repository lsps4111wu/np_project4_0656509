#pragma once

#include <list>
#include <sys/types.h>

typedef void (*Callback)(void*, ssize_t n);
struct Task
{
	enum Type {Read_type, Write_type} type;

	int fd;
	Callback callback_func;
	void *callback_arg;
	void *buf;
	size_t size;
	ssize_t current_finish_size;
};

class TaskList {
public:
	

	TaskList() : maxfd(0){}

	void addReadTask(int , void*, size_t , Callback , void*); 
	void addWriteTask(int , void*, size_t , Callback , void*); 
	void loop();//let task 

	void addTask(Task::Type, int, void*, size_t , Callback , void*);
	int setCheckSet(fd_set*, fd_set*);
	void checkDone(fd_set*, fd_set*);

private:
	std::list<Task> task_list;
	int maxfd;
};
