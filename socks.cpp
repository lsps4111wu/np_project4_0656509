#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <errno.h>
using namespace std;
#define BUFSIZE 60000


void handle_socket(int browser_fd,const struct sockaddr_in& cli_addr)
{
    unsigned char browser_request[BUFSIZE];
    unsigned char reply[8];
    
    int read_num = read(browser_fd, browser_request, BUFSIZE);
    if( read_num<= 0) exit(0);

    unsigned char VN = browser_request[0];
    unsigned char CD = browser_request[1];
    unsigned int DST_PORT = browser_request[2] << 8 | browser_request[3];
    unsigned int DST_IP = browser_request[7] << 24 | browser_request[6] << 16 | browser_request[5] << 8 | browser_request[4];
    
    unsigned char* USER_ID = browser_request +8;

    if(VN!=0x04){
        cout<<"not socks4 request\n";
        exit(0);
    } else {
        cout<<"is socks4 request\n";
    }

    char rule[10];
    char mode [10];
    char addr_str[20];
    unsigned char addr[4]; //Permit IP
    
    FILE* firewall_conf = fopen("socks.conf","r");
    if(!firewall_conf){
        perror("socks.conf not found");
        exit(-1);
    }
    //else cout<<"socks.conf found\n"<<flush;

    fscanf(firewall_conf,"%s %s %s",rule,mode,addr_str);
    char* p;
    while(!feof(firewall_conf)){
        p = strtok(addr_str,".");
        addr[0] = (unsigned char) atoi(p);
        p = strtok(NULL,".");
        addr[1] = (unsigned char) atoi(p);
        p = strtok(NULL,".");
        addr[2] = (unsigned char) atoi(p);
        p = strtok(NULL,".");
        addr[3] = (unsigned char) atoi(p);
        for(int i=0;i<4;++i) cout<<"num ="<<(int)addr[i]<<endl;
        //printf("%s %s %u.%u.%u.%u\n", rule,mode,addr[0],addr[1],addr[2],addr[3]);

        if( (!strcmp(mode,"c") && CD == 0x01) || (!strcmp(mode,"b") && CD == 0x02)){//is connect/bind mode
            if( ( (addr[0] == browser_request[4]) || (addr[0] == 0x00) ) 
                && ( (addr[1] == browser_request[5]) || (addr[1] == 0x00) )
                && ( (addr[2] == browser_request[6]) || (addr[2] == 0x00) )
                && ( (addr[3] == browser_request[7]) || (addr[3] == 0x00) )
                ){
                printf("firewall rule : accept\n");
                reply[1] = 0x5A;//let CD = 90
                break;
            }
        } else {
            reply[1] = 0x5B;//let CD = 91
        }

        fscanf(firewall_conf,"%s %s %s",rule,mode,addr_str);
    }//while loop end
    
    cout<<"--------------------------------------------\n";
    char ip_buf[20];
    int port = ntohs(cli_addr.sin_port);
    inet_ntop(AF_INET, &cli_addr.sin_addr, ip_buf, sizeof(ip_buf));
    cout << "S_IP = " << ip_buf << endl;
    cout << "S_PORT = " << port << endl;
    printf("VN = %u, CD = %u(%s), \n",VN,CD,(CD==0x01? "CONNECT":"BIND") );
    cout<<"DST_IP "<<(int)browser_request[4]<<'.'<<(int)browser_request[5]<<'.'<<(int)browser_request[6]<<'.'<<(int)browser_request[7]
        <<"\nDST_PORT = "<<DST_PORT<<endl;
    printf("Firewall Rule Reply = %s\n", reply[1] == 0x5A ? "Accept":"Deny");
    if(reply[1]==0x5A){
        printf("The rule is : %s %s %u.%u.%u.%u\n", rule,mode,addr[0],addr[1],addr[2],addr[3]);
    }
    cout<<"--------------------------------------------\n";

    if(reply[1] == 0x5B){
        reply[0] = 0;
        for(int i=2;i<8;++i) reply[i] = browser_request[i];
        write(browser_fd,reply,8);
        exit(0);
    }

    if(CD == 0x01){
        cout<<"Command : Connect mode\n";
        reply[0] = 0;
        //reply[1] = 0x5A;
        for(int i=2;i<8;++i) reply[i] = browser_request[i];
        write(browser_fd,reply,8);

        struct sockaddr_in web_serv_addr;
        int web_fd = socket(AF_INET,SOCK_STREAM,0);
        bzero((char*) &web_serv_addr,sizeof(web_serv_addr));
        web_serv_addr.sin_family = AF_INET;
        web_serv_addr.sin_addr.s_addr = DST_IP;
        web_serv_addr.sin_port = htons(DST_PORT);

        if(connect(web_fd,(struct sockaddr *)&web_serv_addr,sizeof(web_serv_addr)) == -1){
            perror("Connect to web failed");
            exit(0);
        } else {
            cout<<"Connect success\n";
        }

        
        fd_set rfds,afds;
        int nfds = max(browser_fd,web_fd) +1;
        char buffer[BUFSIZE];

        FD_ZERO(&afds);
        FD_SET(web_fd,&afds);
        FD_SET(browser_fd,&afds);

        while(1){
            FD_ZERO(&rfds);
            rfds = afds;

            //cout<<"b4 select\n";
            if(select(nfds,&rfds,NULL,NULL,NULL) < 0) {
                perror("select failed : ");
                exit(0);
            }
            //cout<<"af select\n";

            int cnt;
            if(FD_ISSET(browser_fd,&rfds)){
                memset(buffer,0,BUFSIZE);
                cnt = read(browser_fd,buffer,BUFSIZE);
                cout<<"read cnt = "<<cnt<<endl;
                if(cnt == 0){
                    cout<<"browser_fd end\n";
                    exit(0);
                } else if(cnt == -1){
                    cout<<"browser_fd ERROR\n";
                    exit(0);
                } else {
                    cnt = write(web_fd,buffer,cnt);
                }

            }

            if(FD_ISSET(web_fd,&rfds)){
                memset(buffer,'\0',BUFSIZE);
                cnt = read(web_fd,buffer,BUFSIZE);
                cout<<"read cnt = "<<cnt<<endl;
                if(cnt == 0){
                    cout<<"web_fd end\n";
                    exit(0);
                } else if(cnt == -1){
                    cout<<"web_fd ERROR\n";
                    exit(0);
                } else {
                    cnt = write(browser_fd,buffer,cnt);
                }
            }

            cout<<endl;
        }
    } else if(CD == 0x02){
        cout<<"Command : Bind mode\n";
        struct sockaddr_in bind_addr;

        int bind_fd = socket(AF_INET,SOCK_STREAM,0);
        if(bind_fd<0){
            perror("bind error : ");
            exit(0);
        }

        bind_addr.sin_family = AF_INET;
        bind_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        bind_addr.sin_port = htons(INADDR_ANY);
        
        if (bind(bind_fd, (struct sockaddr *) &bind_addr, sizeof(bind_addr)) < 0){
            perror("bind : can't bind local address");
            exit(-1);
        }

        cout<<"bind fd = "<<bind_fd<<endl;
        struct sockaddr_in sa;
        unsigned sa_len = sizeof(sa);
        int rc = getsockname(bind_fd,(struct sockaddr*) &sa,&sa_len);
        if(rc == -1){
            perror("getsockname error : ");
        }

        if(listen(bind_fd,5) < 0){
            perror("listen : can't listen");
        }

        
        reply[0] = 0;
        //reply[1] = 0x5A;
        reply[2] = (unsigned char) (ntohs(sa.sin_port)/256);
        reply[3] = (unsigned char) (ntohs(sa.sin_port)%256);
        for(int i=4;i<8;++i) reply[i] = 0;

        write(browser_fd,reply,8);

        struct sockaddr_in ftp_addr;
        unsigned ftp_len = sizeof(ftp_addr);
        int ftp_fd = accept(bind_fd,(struct sockaddr*) &ftp_addr, (socklen_t*) &ftp_len);
        if(ftp_fd < 0){
            perror("ftp_fd : accept error : ");
            exit(-1);
        }

        write(browser_fd,reply,8);//why need this??

        fd_set afds,rfds;
        int nfds = max(ftp_fd,browser_fd) +1;

        FD_ZERO(&afds);
        FD_SET(ftp_fd,&afds);
        FD_SET(browser_fd,&afds);

        char buffer[BUFSIZE];
        while(1){
            FD_ZERO(&rfds);
            rfds = afds;

            if(select(nfds,&rfds,NULL,NULL,NULL) < 0){
                perror("select failed : ");
                exit(-1);
            }

            int cnt;
            if(FD_ISSET(browser_fd,&rfds)){
                memset(buffer,0,BUFSIZE);
                cnt = read(browser_fd,buffer,BUFSIZE);
                cout<<"Read from browser_fd\n";
                cout<<"read cnt = "<<cnt<<endl;
                if(cnt == 0){
                    cout<<"browser_fd end\n";
                    exit(0);
                } else if(cnt == -1){
                    cout<<"browser_fd ERROR\n";
                    exit(0);
                } else {
                    cnt = write(ftp_fd,buffer,cnt);
                }
            }

            if(FD_ISSET(ftp_fd,&rfds)){
                memset(buffer,0,BUFSIZE);
                cnt = read(ftp_fd,buffer,BUFSIZE);
                cout<<"Read from ftp_fd\n";
                cout<<"read cnt = "<<cnt<<endl;
                if(cnt == 0){
                    cout<<"ftp_fd end\n";
                    exit(0);
                } else if(cnt == -1){
                    cout<<"ftp_fd ERROR\n";
                    exit(0);
                } else {
                    cnt = write(browser_fd,buffer,cnt);
                }
            }

            cout<<endl;
        }
    }

    
    exit(0);
}

int main(int argc, char **argv)
{
    int i, pid, listenfd, newsockfd;
    struct sockaddr_in cli_addr,serv_addr;
    
    if(chdir(".") == -1){ 
        printf("ERROR: Can't Change to directory %s\n",argv[2]);
        exit(4);
    }
    
    /* 讓父行程不必等待子行程結束 */
    signal(SIGCLD, SIG_IGN);

    /* 開啟網路 Socket */
    if ((listenfd = socket(AF_INET, SOCK_STREAM,0))<0)
        exit(3);
    
    /* 網路連線設定 */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons( atoi(argv[1]) );

    int optval = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
    /* 開啟網路監聽器 */
    if (bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        perror("server: can't bind local address");
        exit(-1);
    }
    
    /* 開始監聽網路 */
    if (listen(listenfd,100) < 0){
        perror("server: can't listen");
        exit(3);
    }
    
    
    while(1) {
        unsigned clilen = sizeof(cli_addr);
        cout<<"waiting...\n\n";
        newsockfd = accept(listenfd, (struct sockaddr *) &cli_addr, &clilen);
        cout<<"accepted, new fd = "<<newsockfd<<endl;
        
        if (newsockfd < 0) {
            perror("server: accept error");
            exit(1);
        }

        //demo spec : firewall 
        char ip_buf[20];
        inet_ntop(AF_INET, &cli_addr.sin_addr, ip_buf, sizeof(ip_buf));
        
        //setenv("REMOTE_ADDR",ip_buf,1);
        //setenv("REMOTE_HOST",ip_buf,1);
        
        /* 分出子行程處理要求 */
        if ((pid = fork()) < 0) {
        	perror("fork error:");
            exit(3);
        } else {
            if (pid == 0) {  /* 子行程 */
                cout<<"close listenfd in child\n";
                close(listenfd);

                handle_socket(newsockfd,cli_addr);
                exit(0);
            } else { /* 父行程 */
            	//wait(NULL);
            	cout<<"close newsockfd in parent\n";
                close(newsockfd);
            }
        }
    }
}