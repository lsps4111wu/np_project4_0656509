#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <errno.h>
using namespace std;
#define BUFSIZE 65536



void set_request_env(char* method,char* path,char* qry_str) {
	setenv("REQUEST_METHOD",method,1);
	setenv("SCRIPT_NAME",path,1);
	setenv("QUERY_STRING",qry_str,1);

	setenv("CONTENT_LENGTH","0",1);
	setenv("REMOTE_HOST","",1);//use "" instead of NULL!!!
	setenv("REMOTE_ADDR","",1);

	setenv("AUTH_TYPE","Anonymous",1);
	setenv("REMOTE_USER","Anonymous",1);
	setenv("REMOTE_IDENT","Not_looking_up",1);
}


void exec_cgi(int fd,char* method,char* path,char* qry_str)
{
	//fork child to do cgi
	if(fork() == 0){
		chdir(".");
		set_request_env(method,path,qry_str);
		string exe("."+string(path));
		cout<<"exe = "<<exe<<endl<<flush;//   ./hw3.cgi

		dup2(fd, STDIN_FILENO);
		dup2(fd, STDOUT_FILENO);
		dup2(fd, STDERR_FILENO);
		//close(fd);
		
		//path +1 = "hw3.cgi"
		execl(exe.c_str(),path+1,NULL);
		cout<<"exec failed\n"<<endl;
	}

}

void read_file_size(ifstream& myfile,int& length)
{
	streampos begin,end;
	
	begin = myfile.tellg();
	myfile.seekg (0, ios::end);
	end = myfile.tellg();
	length = end - begin;
	myfile.seekg (0, ios::beg);
}

void handle_socket(int fd)
{
    //GET /hw3.cgi?h1=140.113.216.36&p1=9999&f1=t1.txt HTTP/1.1
    
    char recv_msg[BUFSIZE] = {0};
    int read_num = read(fd, recv_msg, BUFSIZE-1);
    if( read_num<= 0) return;
    cout<<"recv_msg = "<<recv_msg<<endl<<flush;

    //clear '\n' '\r';
    for (int i=0; i<read_num; ++i) 
        if (recv_msg[i]=='\r'||recv_msg[i]=='\n')
            recv_msg[i] = 0;

    //get the parameters
    char* method = strtok(recv_msg, " ");//GET

    char* path = strtok(NULL, " ");//path = /hw3.cgi?h1=140.113.216.36&p1=9999&f1=t1.txt
    path = strtok(path, "?");//path = /hw3.cgi
    cout<<"path +1 = "<<path+1<<endl;
    char* qry_str = strtok(NULL, " ");//h1=140.113.216.36&p1=9999&f1=t1.txt
    if (qry_str == NULL)
        qry_str = "";
    cout<<"get qry_str: "<<qry_str<<endl<<flush;

    //GET method only
    if( strcmp(method, "GET") ) {
        write(fd, "HTTP/1.0 400 Bad Request\r\n", 26);
        exit(0);
	} 
	cout<<"is GET method\n"<<flush;

	//Let's check the request filename
	char file_path [1+strlen(path)+1];
	cout<<"alocate file_path space done\n"<<flush;

	sprintf(file_path,".%s",path);
	cout<<"file path get\n"<<flush;

	if(strlen(file_path)>4 && !strcmp(file_path + strlen(file_path) -4, ".cgi") ){//this is cgi request~
		//check cgi file's property and exist or not
		//stat's return == 0 is "open failed"
		cout<<"this is cgi request~\n"<<flush;
		struct stat sb;

		if (stat(file_path, &sb) != 0 || (sb.st_mode & S_IFMT) != S_IFREG) {//S_IFREG is regular file
			write(fd, "HTTP/1.0 404 Not Found\r\n\r\n", 26);
		} else if (!sb.st_mode & S_IRUSR) {//S_IRUSR is read permission
			write(fd, "HTTP/1.0 403 Forbidden\r\n\r\n", 26);
		} else if (sb.st_mode & S_IXUSR) {//S_IRUSR is execute permission
			cout<<"HTTP server is going to run cgi\n";
			write(fd, "HTTP/1.0 200 OK\r\n", 17);
			exec_cgi(fd, method,path,qry_str);
		} else {
			write(fd, "HTTP/1.0 500 Internal Server Error\r\n\r\n", 38);
		}
	} else {//not cgi file request, eg: html
		cout<<"this is normal file request~\n"<<endl;
		ifstream myfile (file_path, ios::binary);
		if(!myfile){
			write(fd, "HTTP/1.0 404 Not Found\r\n\r\n", 27);
		} else {
			int length;
			read_file_size(myfile, length);
			cout<<"file_length = "<<length<<endl<<flush;
			char buf[120];
			int header_length = sprintf(buf, "HTTP/1.0 200 OK\r\nContent-Length: %d\r\nContent-Type: %s\r\n\r\n", length, "text/html");
			write(fd, buf, header_length);
			cout<<"buf = "<<buf<<endl<<flush;
			char resp[length];
			myfile.read(resp,length);
			cout<<"resp = "<<resp<<endl<<flush;
			write(fd, resp, length);
			
		}
		
		
	}


    exit(0);
}

int main(int argc, char **argv)
{
    int i, pid, listenfd, newsockfd;
    struct sockaddr_in cli_addr,serv_addr;
    
    if(chdir(".") == -1){ 
        printf("ERROR: Can't Change to directory %s\n",argv[2]);
        exit(4);
    }
    cout<<"1\n";
    /* 背景繼續執行 */
    /*if(fork() != 0)
        return 0;*/
    /* 讓父行程不必等待子行程結束 */
    signal(SIGCLD, SIG_IGN);

    /* 開啟網路 Socket */
    if ((listenfd = socket(AF_INET, SOCK_STREAM,0))<0)
        exit(3);
    cout<<"1\n";
    /* 網路連線設定 */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons( atoi(argv[1]) );

    int optval = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
    /* 開啟網路監聽器 */
    if (bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        perror("server: can't bind local address");
    cout<<"1\n";
    /* 開始監聽網路 */
    if (listen(listenfd,5) < 0)
        exit(3);
    cout<<"1\n";
    while(1) {
        unsigned clilen = sizeof(cli_addr);
        cout<<"waiting...\n";
        newsockfd = accept(listenfd, (struct sockaddr *) &cli_addr, &clilen);
        cout<<"accepted, new fd = "<<newsockfd<<endl;
        
        if (newsockfd < 0) {
            perror("server: accept error");

            continue;
        }

        //demo spec : firewall 
        char ip_buf[20];
        int port = ntohs(cli_addr.sin_port);
        inet_ntop(AF_INET, &cli_addr.sin_addr, ip_buf, sizeof(ip_buf));
        cout << "IP = " << ip_buf << endl;
        cout << "port = " << port << endl;
        cout << "this_fd = " << newsockfd << endl;
        //if (strncmp(ip_buf, "42.73", 5) == 0) { 
        /*if (strncmp(ip_buf, "140.113.167", 11) == 0) { 
            printf("it matches! OK\n"); 
        }
        else { 
            write(newsockfd, "HTTP/1.0 403 Forbidden\r\n\r\n", 26); 
            close(newsockfd);
            continue;
        }*/
        //demo spec end
        
        /* 分出子行程處理要求 */
        if ((pid = fork()) < 0) {
        	perror("fork error:");
            exit(3);
        } else {
            if (pid == 0) {  /* 子行程 */
                cout<<"close listenfd in child\n";
                close(listenfd);

                handle_socket(newsockfd);
                exit(0);
            } else { /* 父行程 */
            	wait(NULL);
            	cout<<"close newsockfd in parent\n";
                close(newsockfd);
            }
        }
    }
}
